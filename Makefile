CXX=g++
CXXFLAGS=-O3 -Wall -std=c++0x
CXXLIBS=-lm
DEPS=tools.hpp
OBJ_QUEENS=queens.o tools.o
OBJ_LANGFORD=langford.o tools.o

all: queens langford

queens: $(OBJ_QUEENS)
	$(CXX) -o $@ $^ $(CXXFLAGS) $(CXXLIBS)

langford: $(OBJ_LANGFORD)
	$(CXX) -o $@ $^ $(CXXFLAGS) $(CXXLIBS)

%.o: %.c $(DEPS)
	$(CXX) -c -o $@ $< $(CXXFLAGS) $(CXXLIBS)

clean:
	rm -rf queens langford *.o

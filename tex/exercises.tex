\documentclass[12pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8x]{inputenc}
%\usepackage[brazilian]{babel}
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage{subcaption}
\usepackage{siunitx}
\usepackage{geometry}
\usepackage{algorithm2e}

\title{Backtracking Exercises}
\author{Gabriel Mattos Langeloh}

\begin{document}

\maketitle

\textbf{Exercise 5:}

The recursive formulation of Algorithm B is presented in Algorithm
\ref{algbacktracking}.

\DontPrintSemicolon
\SetKwProg{FRecurs}{function}{}{}
\begin{algorithm}[H]
  \FRecurs{try($l$)}{
    \If{$l > n$}{
      print($x_1\ldots x_n$)\;
      \KwRet{}
    }
    \For{$x \in D_l$}{
      \If{$P(x_1\ldots x_{l-1}x)$}{
        $x_l$ = x\;
        try($l$)\;
      }
    }
  }
  \caption{Basic recursive backtracking}
  \label{algbacktracking}
\end{algorithm}

The reason backtracking wasn't presented recursively in the book is likely
for efficiency reasons --- most optimizations presented later, such as
Algorithm B* and Algorithm W would probably have to be presented in iterative
form anyway.

Implementation available:
\url{https://bitbucket.org/gmlangeloh/knuth4b/src/master/}. Just run ``python3
recursive\_backtrack n''.

\textbf{Exercise 10:} Store vectors (indexed by the level $l$ of the tree)
corresponding to the sets $S_l, A_l, B_l, C_l$. These sets can be represented by
bitstrings that fit in a single machine word for reasonable values of $n$ (we
need $n$ bits). $S_l$ can, by definition, be updated by
\[
  s_l = \mu \wedge \overline{a_l} \wedge \overline{b_l} \wedge \overline{c_l}
\] where $\mu$ is a mask with its lowest $n$ bits $1$. This can be done in step
W2 of Walker's algorithm.

To take the minimum of $S_l$ it suffices to compute $t = s_l \wedge (-s_l)$,
because in 2's complement the only bit that is the same in both $s_l$ and $-s_l$
is the first bit where a carry occurs, thus the least significant bit $1$ of
$s_l$.

Then, we can update $a_l = a_{l-1} + t$ (add column to $a$),
$b_l = (b_{l-1} + t) \gg 1$ (add diagonal to $b$ and move every diagonal left
for the next level), $c_l = (c_{l-1} + t \ll 1) \wedge \mu$ (add diagonal to $c$
and move right for the next level, removing diagonals outisde of the board with
a mask) and $s_l = s_l - t$ (remove $t$ from $s_l$). This should be done in step
W3 to update the data structures.

Everything here can be done in $O(1)$.

Implementation available:
\url{https://bitbucket.org/gmlangeloh/knuth4b/src/master/}. Just compile with
make and run ``./queens walker n''.

\textbf{Exercise 17:} No, because then it is impossible to backtrack
efficiently. When backtracking, one must skip all negative numbers. If they are
made positive instead, the algorithm either doesn't skip anything (if we leave
the condition as while $x_l < 0$ as is) or it deletes the entire partial
solution (if we change the condition to while $x_l \neq 0$). The alternative
would be knowing for each $k$ such that $x_l = k$ whether there is some $j < l$
with $x_j = k$ as well. In particular, this could be done by inverting the
formula (7), but it is easier to just leave the negative numbers, as there is no
advantage to removing them.

\textbf{Exercise 18:} By definition of the $x_k$, the current solution is
$x = 241\overline{2}\overline{1}0\overline{4}0$. The linked list $p$ and the
backtracking table $y$ can be computed level by level. Intuitively, $p$ is the
ordered linked list of unused positive numbers and $y$ records the removal from
$p$ that was made at each level.

At the first level, before setting any variable, $p = 12340$. Then, $2$ is
removed from $p$, so $p = 13340$ and $y_1$ is modified to point at whatever was
pointing to $2$, so $y = 100$. Next, $4$ is removed from $p$, so $p = 13300$ and
$y = 130$. Finally, $1$ is removed from $p$, so $p = 33300$ and $y = 130$.

\textbf{Exercise 19:} $D_l = \{k \in \mathbb{Z} \mid -n \leq k \leq n, k \neq
0\}$.

\textbf{Exercise 20:} First we need to query in $O(1)$ whether a given positive
integer is already in the solution (or equivalently, is still in the list $p$)
or not. To do that, we can keep a boolean array indexed by $[n]$. Then, note
that at level $l$, the element $k = 2n - l - 1$ has to be put into the solution,
else its negative will have index $2n - l - 1 + j + 1 > 2n$ for $j > l$.
However, this only makes sense for $l \geq n - 1$, as that ensures $k \leq n$
(and so $k$ is a positive integer).

In step L2, when we skip the previously set negative $x_l$, we must now also
check whether we can detect that $k = 2n - l - 1$ cannot be put into the
solution anymore. If so, we can prune the tree and backtrack immediately. This
can be done in $O(1)$ by simply checking in the while loop whether this value of
$k$ is in the boolean array.

In step L3, before trying to set a variable, we can verify whether $k = 2n - l -
1$, for $l \geq n - 1$, has been set. If it hasn't, it must be set now, and so
we walk through the unused list $p$ updating the pointer $k$. Then, we proceed
setting variables normally.

Implementation available:
\url{https://bitbucket.org/gmlangeloh/knuth4b/src/master/}. Just compile with
make and run ``./langford langford2 n''.

\textbf{Exercise 21:}

First define the ``dual index function'' \[
  D: i \mapsto 2n - i + 1.
\]

Now, to solve (a), note that
\[
  k \leq \lfloor n/2 \rfloor \iff -k \geq -\lfloor n/2 \rfloor
  \iff n - k \geq n - \lfloor n/2 \rfloor = \lceil n/2 \rceil > \lfloor n/2 \rfloor
\] with the last inequality following from the fact that $n$ is odd. Also, if
$x_k = n$ then $x_{n + k + 1} = -n$, so that the index of $n$ in $x^D$ is $D(n +
k + 1) = n - k$. The result follows.

The reasoning for (b) is similar:
\[
  k \leq n/2 \iff n - k \geq n/2 \iff n - k + 1 > n/2
\] noting that, in order to break symmetries, we need to distinguish between the
``primal'' and ``dual'' symmetric solutions. For this reason, we must force the
last inequality $n - k + 1 > n/2$ to be strict by adding $1$ to the left-hand
side. Now, in order to obtain a dual index of $n - k + 1$ we can start with
$x_k = n - 1$ and apply the dual index function $D$ as before. This proves that
$x_k = n - 1$ for some $k \leq n/2$ if and only if $x^D_{n - k + 1} = n - 1$,
with $n - k + 1 > n/2$.

We can use (a) and (b) to develop an algorithm breaking symmetries and solve
(c). Define $n' = n - (n \mod 2)$ and say a solution $x$ is ``primal'' if there
is some $k \leq \lfloor n/2 \rfloor$ such that $x_k = n'$, and ``dual''
otherwise (that is, $n'$ appears after index $\lfloor n/2 \rfloor$ in $x$). By
(a) and (b), this partitions all solutions. We can force the backtracking
algorithm to visit only primal solutions, for example. To do this, backtrack
whenever, in step L2, $n'$ was not set until index $\lfloor n/2 \rfloor$.
Additionally, in step L3, force $x_k = n'$ when $k = \lfloor n/2 \rfloor$
similarly to Exercise 20.

Implementation available:
\url{https://bitbucket.org/gmlangeloh/knuth4b/src/master/}. Just compile with
make and run ``./langford langford3 n''.

\textbf{Total difficulty: 139}

\end{document}
\documentclass[12pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8x]{inputenc}
%\usepackage[brazilian]{babel}
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage{subcaption}
\usepackage{siunitx}
\usepackage{geometry}

\title{Knuth's TAOCP 7.2.2: Backtracking}
\author{Gabriel Mattos Langeloh}

\begin{document}

\maketitle

This document shows some experimental data and additional information on Knuth's 
TAOCP 7.2.2. All experiments use the code available at

\begin{center}
\url{https://bitbucket.org/gmlangeloh/knuth4b/src/master/}.
\end{center}

\section{Queens}

The number of solutions of this problem is the sequence
\href{https://oeis.org/A000170}{OEIS A000170}:

\begin{align*}
  &1, 1, 0, 0, 2, 10, 4, 40, 92, 352, 724, 2680, 14200, 73712, 365596, 2279184, 14772512, \\
  &95815104, 666090624, 4968057848, 39029188884, 314666222712, 2691008701644, \\
  &24233937684440, 227514171973736, 2207893435808352, 22317699616364044, \\
  &234907967154122528, \ldots
\end{align*}

See Table \ref{tablequeens} for some experimental results comparing the
algorithms Brute (brute force search), B (generic backtracking), B*
(backtracking with specialized data structures) and W (Walker's backtracking).

\begin{table}[ht]
\centering
\resizebox{\textwidth}{!}{
\begin{tabular}{lrrrrrr}
  \toprule
  & \multicolumn{2}{c}{Brute} & \multicolumn{2}{c}{B} & \multicolumn{1}{c}{B*} & \multicolumn{1}{c}{W} \\
  \cmidrule(r){2-3} \cmidrule(r){4-5} \cmidrule(r){6-6} \cmidrule(r){7-7}
  $n$ & $t$ & $N$ & $t$ & $N$ & $t$ & $t$ \\ 
  \midrule
  1 & \num{2.6e-06} & 3 & \num{1.8e-06} & 2 & \num{2.1e-06} & \num{1.3e-06} \\ 
  2 & \num{2.2e-06} & 7 & \num{1.7e-06} & 3 & \num{1.9e-06} & \num{1.8e-06} \\ 
  3 & \num{4.7e-06} & 40 & \num{1.8e-06} & 6 & \num{2.2e-06} & \num{1.4e-06} \\ 
  4 & \num{1.0e-05} & 343 & \num{2.5e-06} & 17 & \num{2.6e-06} & \num{1.5e-06} \\ 
  5 & \num{6.3e-05} & 3916 & \num{4.4e-06} & 54 & \num{4.1e-06} & \num{2.5e-06} \\ 
  6 & \num{7.9e-04} & 55991 & \num{1.4e-05} & 153 & \num{9.3e-06} & \num{2.7e-06} \\ 
  7 & \num{9.3e-03} & 960840 & \num{5.0e-05} & 552 & \num{2.8e-05} & \num{6.4e-06} \\ 
  8 & \num{1.9e-01} & 19174053 & \num{1.7e-04} & 2057 & \num{1.0e-04} & \num{1.6e-05} \\ 
  9 & \num{4.56} & 435848402 & \num{7.8e-04} & 8394 & \num{4.7e-04} & \num{5.8e-05} \\ 
  10 & \num{121.61} & 11111111835 & \num{3.6e-03} & 35539 & \num{2.1e-03} & \num{2.64e-04} \\ 
  11 & \num{>300.0} & NA & \num{1.7e-02} & 166926 & \num{1.0e-02} & \num{1.32e-03} \\ 
  12 & \num{>300.0} & NA & \num{9.4e-02} & 856189 & \num{5.6e-02} & \num{6.8e-03} \\ 
  13 & \num{>300.0} & NA & \num{5.4e-01} & 4674890 & \num{3.1e-01} & \num{3.6e-02} \\ 
  14 & \num{>300.0} & NA & 3.38 & 27358553 & 1.85 & \num{2.1e-01} \\ 
  15 & \num{>300.0} & NA & 22.43 & 171129072 & 11.99 & 1.37 \\ 
  16 & \num{>300.0} & NA & 160.01 & 1141190303 & 82.29 & 8.65 \\ 
  \bottomrule
\end{tabular}}
  \caption{Time (t) and nodes (N) in the tree for each algorithm for the Queens
    problem. N is the same
    for algorithms B, B* and W.}
  \label{tablequeens}
\end{table}

\section{Langford pairs}

The number of solutions of this problem is the sequence
\href{https://oeis.org/A014552}{OEIS A014552} (each dual pair of solutions is
only counted once):

\begin{align*}
  &0, 0, 1, 1, 0, 0, 26, 150, 0, 0, 17792, 108144, 0, 0, 39809640, 326721800, 0, 0, \\
  &256814891280, 2636337861200, 0, 0, 3799455942515488, 46845158056515936, \\
  &0, 0, 111683611098764903232, 1607383260609382393152, \ldots
\end{align*}

Table \ref{tablelangford} shows results of three backtracking algorithms for the
Langford pairs problem. L1 is algorithm L in TAOCP, L2 is algorithm L1 with
the optimizations given by Exercise 20 and L3 is L2 with the additional
optimizations of Exercise 21.

\begin{table}[h]
\centering
\resizebox{\textwidth}{!}{
\begin{tabular}{lrrrrrr}
  \toprule
  & \multicolumn{2}{c}{L1} & \multicolumn{2}{c}{L2} & \multicolumn{2}{c}{L3} \\
  \cmidrule(r){2-3} \cmidrule(r){4-5} \cmidrule(r){6-7}
  $n$ & $t$ & $N$ & $t$ & $N$ & $t$ & $N$ \\ 
  \midrule
  1 & \num{7.8e-06} & 1 & \num{5.8e-06} & 1 & \num{3.9e-06} & 1 \\
  2 & \num{5.4e-06} & 3 & \num{6.1e-06} & 2 & \num{6.2e-06} & 2 \\
  3 & \num{8.0e-06} & 10 & \num{6.3e-06} & 9 & \num{4.8e-06} & 4 \\
  4 & \num{7.6e-06} & 27 & \num{6.7e-06} & 23 & \num{7.7e-06} & 13 \\
  5 & \num{1.5e-05} & 84 & \num{1.2e-05} & 67 & \num{1.0e-05} & 33 \\
  6 & \num{2.3e-05} & 342 & \num{2.3e-05} & 260 & \num{1.1e-05} & 136 \\
  7 & \num{6.9e-05} & 1630 & \num{1.0e-04} & 1157 & \num{5.6e-05} & 578 \\
  8 & \num{5.5e-04} & 8787 & \num{4.3e-04} & 5672 & \num{1.4e-04} & 150 \\
  9 & \num{1.9e-03} & 50759 & \num{1.2e-03} & 29323 & \num{7.1e-04} & 14261 \\
  10 & \num{1.2e-02} & 326781 & \num{7.8e-03} & 171981 & \num{4.1e-03} & 80674 \\
  11 & \num{8.6e-02} & 23358881 & \num{4.9e-02} & 1130017 & \num{2.5e-02} & 546445 \\
  12 & \num{6.3e-01} & 17896497 & \num{3.3e-01} & 7811653 & \num{1.6e-01} & 3660632 \\
  13 & 5.24 & 146791097 & 2.53 & 57019103 & 1.22 & 27642069 \\
  14 & 46.05 & 1294581103 & 19.6129 & 453869965 & 9.85 & 213433291 \\
  \bottomrule
\end{tabular}}
\caption{Time (t) and nodes (N) in the tree for each algorithm for the Langford
  pairs problem.}
\label{tablelangford}
\end{table}

\section{Estimates}

Table \ref{tableestimates} provides the statistics for the estimation of the
number of nodes in the backtracking tree for the Queen's problem.

\begin{table}[h]
  \centering
  \resizebox{\textwidth}{!}{
    \begin{tabular}{lrrrrr}
      \toprule
      $n$ & min & avg & max & std & $N$ \\
      \midrule
      1 & 2 & 2.00 & 2 & 0.00 & 2 \\
      2 & 3 & 3.00 & 3 & 0.00 & 3 \\
      3 & 4 & 5.83 & 7 & 1.46 & 6 \\
      4 & 13 & 16.69 & 21 & 2.95 & 17 \\
      5 & 36 & 53.34 & 76 & 12.61 & 54 \\
      6 & 97 & 153.51 & 421 & 50.70 & 153 \\
      7 & 253 & 551.55 & 1618 & 246.141 & 552\\
      8 & 489 & 2052.38 & 7409 & 1067.44 & 2057\\
      9 & 928 & 8376.32 & 60760 & 4871.75 & 8394\\
      10 & 1831 & 35507.20 & 317181 & 23057.40 & 35539\\
      11 & 9340 & 166566.00 & 2144396 & 119049.00 & 166926\\
      12 & 19777 & 855965.00 & 13971757 & 670342.00 & 856189\\
      13 & 38624 & \num{4.66141e+06} & 110604274 & \num{3.98916e+06} & 4674890\\
      14 & 166489 & \num{2.73267e+07} & 706763149 & \num{2.56082e+07} & 27358553\\
      15 & 952396 & \num{1.7071e+08} & 4240345336 & \num{1.70997e+08} & 171129072\\
      16 & 2597105 & \num{1.13985e+09} & 36520342785 & \num{1.05934e+09} & 1141190303\\
      17 & 6809674 & \num{7.99991e+09} & 287590929280 & \num{8.02811e+09} & 8017021932\\
      18 & 29993239 & \num{5.91922e+10} & 2436731965369 & \num{5.91951e+10} & 59365844491\\
      \bottomrule
    \end{tabular}
  }
  \caption{Estimates of the number of nodes in the trees of the Queen's problem
    given by 1000000 runs of Algorithm E. $N$ is the true number of nodes.}
  \label{tableestimates}
\end{table}

\end{document}
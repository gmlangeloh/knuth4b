#include "tools.hpp"

#include <iostream>
#include <chrono>
#include <random>
#include <cmath>

void run_algorithm (std::function<void(int, Profile&)> algorithm, int n) {

  //Run the chosen algorithm, but also compute running time
  auto start = std::chrono::high_resolution_clock::now();
  Profile profile(n+1, 0);

  algorithm(n, profile);

  //Print output: profile, time, total nodes and solutions
  auto end = std::chrono::high_resolution_clock::now();
  double time = std::chrono::duration_cast<std::chrono::nanoseconds>
    (end - start).count();

  size_t total_nodes = 0;
  std::cout << "Profile: ";
  for (size_t nodes : profile) {
    std::cout << nodes << " ";
    total_nodes += nodes;
  }
  std::cout << std::endl;

  std::cout << "Nodes: " << total_nodes << std::endl;
  std::cout << "Time: " << (time * 1e-9) << std::endl;
  std::cout << "Solutions: " << profile.back() << std::endl;
}

//Generic cost estimator for backtracking.
//It's not particularly efficient, because it is generic.
//TODO unfortunately not generic enough. We need a construction to
//set variables, for example, in order for Langford to work.
//Probably should use some abstract class or something
size_t estimate_cost (int n, CostFunction c, DegreeFunction deg) {
  //E1: Initialization
  size_t l = 0, D = 1;
  size_t S = 0;

  std::vector<int> y(n, 0); //Strictly, the size of y is the size of the domains
  std::vector<int> X(n, 0);

  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator(seed);

  int d = 0;
  do {
    //E2: Enter level l
    S = S + D * c(X); //Add cost of latest node
    if (l >= unsigned(n)) { //Full solution, break
      break;
    }

    //These steps essentially compute the set S_l of Walker's backtracking
    //E3: Test x
    //E4: Try again
    d = deg(X, y, l);

    //E5: Choose and try
    if (d != 0) {
      D = D * d;
      std::uniform_int_distribution<int> uniform(0, d - 1);
      int I = uniform(generator);
      X[l] = y[I];
      l++;
    }
  } while (d != 0);

  return S;
}

bool solutions (size_t* sols, int n, CostFunction c, DegreeFunction deg) {
  //E1: Initialization
  size_t l = 0, D = 1;
  size_t S = 0;

  std::vector<int> y(n, 0); //Strictly, the size of y is the size of the domains
  std::vector<int> X(n, 0);

  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator(seed);

  int d = 0;
  do {
    //E2: Enter level l
    S = S + D * c(X); //Add cost of latest node
    if (l >= unsigned(n)) { //Full solution, break
      *sols = D / d;
      return true;
    }

    //These steps essentially compute the set S_l of Walker's backtracking
    //E3: Test x
    //E4: Try again
    d = deg(X, y, l);

    //E5: Choose and try
    if (d != 0) {
      D = D * d;
      std::uniform_int_distribution<int> uniform(0, d - 1);
      int I = uniform(generator);
      X[l] = y[I];
      l++;
    }
  } while (d != 0);

  return false;
}

//Just doesn't work. I guess discarding the infeasible solutions is not enough, we need to generate
//directly a tree with only feasible leaves.
void estimate_solutions (int n, int repetitions, CostFunction c,
                         DegreeFunction deg) {
  double s = 0.0;
  for (int i = 0; i < repetitions; i++) {
    size_t sols = 0;
    while ( !solutions(&sols, n, c, deg) ) {}
    s += sols;
  }
  std::cout << s / repetitions << std::endl;
}

size_t constant_cost(std::vector<int>& v) {
  return 1;
}

double deviation(std::vector<size_t>& values, double mean) {
  double s = 0.0;
  for (int val : values) {
    s += (val - mean) * (val - mean);
  }
  return sqrt(s / values.size());
}

void fournum(int n, CostFunction c, DegreeFunction deg, int repetitions) {
  size_t min = std::numeric_limits<size_t>::max();
  size_t max = 0;
  double sum = 0.0;
  double avg, dev;
  std::vector<size_t> values;
  for (int r = 0; r < repetitions; r++) {
    size_t size = estimate_cost(n, c, deg);
    values.push_back(size);
    if (size > max) {
      max = size;
    }
    if (size < min) {
      min = size;
    }
    sum += size;
  }
  avg = sum / repetitions;
  dev = deviation(values, avg);
  std::cout << min << " " << avg << " " << max << " " << dev << std::endl;
}

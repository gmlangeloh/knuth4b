import sys

n = int(sys.argv[1])
sol = [0] * (n+1)

def print_sol():
    print(sol[1:])

def P(x, l):
    for i in range(1, l):
        if sol[i] == x: #Same column
            return False
        if sol[i] - x == i - l: #Same diagonal
            return False
        if x - sol[i] == i - l: #Same diagonal
            return False
    return True

def try_queens(l):
    if l > n:
        print_sol()
        return
    for x in range(1, n+1):
        if P(x, l):
            sol[l] = x
            try_queens(l+1)

try_queens(1)

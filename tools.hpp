#ifndef __TOOLS_H__
#define __TOOLS_H__

#include <functional>
#include <vector>

typedef std::vector<size_t> Profile;
typedef std::function<size_t(std::vector<int>&)> CostFunction;

size_t constant_cost(std::vector<int>&);

typedef std::function<size_t(std::vector<int>&, std::vector<int>&, int)>
DegreeFunction;

void run_algorithm (std::function<void(int, Profile&)> algorithm, int n);
size_t estimate_cost (int n, CostFunction c, DegreeFunction deg);
void fournum(int n, CostFunction c, DegreeFunction deg, int repetitions);
void estimate_solutions (int n, int repetitions, CostFunction c,
                         DegreeFunction deg);

double deviation(std::vector<size_t>& values, double mean);

#endif

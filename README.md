README
======

Implementations of some backtracking algorithms described in Knuth's The Art Of
Computer Programming, Volume 4B.

All programs can be compiled running make.

Queens
------

To run algorithms for the Queens problem, use

    ./queens <algorithm> <n> <repetitions>

where algorithm is one of 

- bruteforce: a brute force algorithm that explores the entire search tree
with no pruning

- backtrack1: a generic backtracking algorithm with no specialized data structures
(Knuth's 7.2.2.B)

- backtrack2: a backtracking algorithm with data structures to check whether a
square is available in O(1) (Knuth's 7.2.2.B\*)

- walker: Walker's backtracking algorithm, generating the set of available squares
in a row in O(1) (assuming n is bounded by the machine word size) 
(Knuth's 7.2.2.W)

- fournum: run Knuth's Algorithm 7.2.2.E repetitions times and return the 
minimum, average, maximum and deviation of the estimated number of nodes in the
backtracking tree

n is the board size and repetitions is the number of runs of the estimation
algorithm (only used in the fournum algorithm).

Langford
--------

To tun algorithms for the Langford pairs problem, use

    ./langford <algorithm> <n>
    
where algorithm is one of

- langford1: a generic backtracking algorithm for Langford corresponding to
Knuth's 7.2.2.L

- langford2: an improvement of langford1 that predicts when a branch wouldn't have to be pursued and cuts it off (Exercise 7.2.2.20)

- langford3: an improvement of langford2 that also halves the tree breaking the
symmetry given by duals (Exercise 7.2.2.21)

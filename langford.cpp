#include <chrono>
#include <cstring>
#include <iostream>
#include <vector>

#include "tools.hpp"

//Prints current solution (permutation) x
void print_solution (std::vector<int>& x) {
  for (int i = 1; unsigned(i) < x.size(); i++) {
    std::cout << "(" << x[i] << ")";
  }
  std::cout << std::endl;
}

//Prints linked list of unused elements represented by vector p, starting at j
void print_unused (std::vector<int>& p, int j) {
  int next = p[j];
  while (next != 0) {
    std::cout << next << " ";
    next = p[next];
  }
  std::cout << std::endl;
}

//Basic Langford pairs backtracking
void langford1 (int n, Profile& profile) {
  std::vector<int> p(n+1, 0);
  //I am going to use 1-indexed arrays for x / y, because 0 needs to be a value
  std::vector<int> y(2*n + 1, 0);
  std::vector<int> x(2*n + 1, 0);

  //2n levels, only 1 node in the first level
  profile.resize(n + 1, 0);
  profile[0] = 1;

  //Initialize "linked list"
  for (int i = 0; i < n; i++) {
    p[i] = i + 1;
  }

  int l = 1, k = 0, j = 0;
  int tree_level = 1; //Variable used to keep track of the level of the tree
  bool new_level = true;
  bool try_value = false;
  bool retry = false;
  bool backtrack = false;
  while (l > 0) {
    //Some good prints for debugging
    //print_solution(x);
    //print_unused(p, j);
    if (new_level) {
      new_level = false;
      k = p[0];
      if (k == 0) { //List of unused numbers ended, finished building solution
        //print_solution(x);
        //solution_count++;
        backtrack = true; //Backtrack to find more solutions
      } else {
        j = 0; //Start from the beginning of unused list
        while (l <= 2*n &&
               x[l] < 0) { //Skip every negative number already put in solution
          l++;
        }
        try_value = true;
      }
    }
    if (try_value) {
      try_value = false;
      if (l + k + 1 > 2*n) {
        //Can't set x[l] = k, its pair would go out of bounds. Need to backtrack.
        backtrack = true;
      } else {
        //Try to set x[l] = k. No immediate reason for this to be a problem.
        if (x[l + k + 1] == 0) { //The position of the pair of x[l] = k is unset
          x[l] = k;
          x[l + k + 1] = -k; //Set its pair in the solution
          y[l] = j; //Record that we removed p[j] = k from the list at level l
          p[j] = p[k]; //Remove p[j] = k from the unused list
          profile[tree_level]++;
          l++;
          tree_level++;
          new_level = true;
        } else {
          //Cannot set x[l] = k, its pair is unavailable
          retry = true;
        }
      }
    }
    if (retry) {
      retry = false;
      j = k;
      k = p[j];
      if (k == 0) { //List ended, backtrack
        backtrack = true;
      } else { //try_value with the new j, k (next value in unused list)
        try_value = true;
      }
    }
    if (backtrack) {
      backtrack = false;
      l--;
      tree_level--;
      if (l > 0) {
        while (l > 0 && x[l] < 0) {
          l--; //Skip/Ignore all negative values in solution
        }
        //Remove x[l] from solution, put it back into the unused list
        k = x[l];
        x[l] = 0;
        x[l + k + 1] = 0;
        j = y[l];
        p[j] = k;
        retry = true;
      }
    }
  }
}

//Backtracking with prediction mechanism: when it is the last chance
//to add a number to the solution, predict that and do so
void langford2 (int n, Profile& profile) {
  std::vector<int> p(n+1, 0);
  //I am going to use 1-indexed arrays for x / y, because 0 needs to be a value
  std::vector<int> y(2*n + 1, 0);
  std::vector<int> x(2*n + 1, 0);

  //Array to check whether a number has been used in O(1)
  //This allows to predict some infeasibilities early
  std::vector<bool> a(n + 1, false);

  //2n levels, only 1 node in the first level
  profile.resize(n + 1, 0);
  profile[0] = 1;

  //Initialize "linked list"
  for (int i = 0; i < n; i++) {
    p[i] = i + 1;
  }

  int l = 1, k = 0, j = 0;
  int tree_level = 1; //Variable used to keep track of the level of the tree
  bool new_level = true;
  bool try_value = false;
  bool retry = false;
  bool backtrack = false;
  while (l > 0) {
    //Some good prints for debugging
    //print_solution(x);
    //print_unused(p, j);
    if (new_level) {
      new_level = false;
      k = p[0];
      if (k == 0) { //List of unused numbers ended, finished building solution
        //print_solution(x);
        //solution_count++;
        backtrack = true; //Backtrack to find more solutions
      } else {
        j = 0; //Start from the beginning of unused list
        try_value = true;
        while (l <= 2*n &&
               x[l] < 0) { //Skip every negative number already put in solution
          if (l >= n - 1 && a[2*n - l - 1] == false) {
            //Don't need to try anything in this case, it won't work!
            //We would skip some negatives and end up too late to put 2*n - l - 1
            try_value = false;
            backtrack = true;
            break;
          } else {
            l++;
          }
        }
      }
    }
    if (try_value) {
      try_value = false;
      if (l + k + 1 > 2*n) {
        //Can't set x[l] = k, its pair would go out of bounds. Need to backtrack.
        backtrack = true;
      } else {
        //Optimization: 2n - l - 1 is not in yet, but this is its last chance
        if (l >= n - 1 && a[2*n - l - 1] == false) {
          while (l + k + 1 != 2*n) {
            j = k;
            k = p[k];
          } //Now k == 2n - l - 1, so we will try to put k in the solution
        }
        //Try to set x[l] = k. No immediate reason for this to be a problem.
        if (x[l + k + 1] == 0) { //The position of the pair of x[l] = k is unset
          x[l] = k;
          x[l + k + 1] = -k; //Set its pair in the solution
          a[k] = true; //Mark as used
          y[l] = j; //Record that we removed p[j] = k from the list at level l
          p[j] = p[k]; //Remove p[j] = k from the unused list
          profile[tree_level]++;
          l++;
          tree_level++;
          new_level = true;
        } else {
          //Cannot set x[l] = k, its pair is unavailable
          retry = true;
        }
      }
    }
    if (retry) {
      retry = false;
      j = k;
      k = p[j];
      if (k == 0) { //List ended, backtrack
        backtrack = true;
      } else { //try_value with the new j, k (next value in unused list)
        try_value = true;
      }
    }
    if (backtrack) {
      backtrack = false;
      l--;
      tree_level--;
      if (l > 0) {
        while (l > 0 && x[l] < 0) {
          l--; //Skip/Ignore all negative values in solution
        }
        //Remove x[l] from solution, put it back into the unused list
        k = x[l];
        x[l] = 0;
        a[k] = false;
        x[l + k + 1] = 0;
        j = y[l];
        p[j] = k;
        retry = true;
      }
    }
  }
}

//This one breaks duality symmetries
void langford3 (int n, Profile& profile) {

  int np = n - (n % 2 == 0); //Used for symmetry breaking

  std::vector<int> p(n+1, 0);
  //I am going to use 1-indexed arrays for x / y, because 0 needs to be a value
  std::vector<int> y(2*n + 1, 0);
  std::vector<int> x(2*n + 1, 0);

  //Array to check whether a number has been used in O(1)
  //This allows to predict some infeasibilities early
  std::vector<bool> a(n + 1, false);

  //2n levels, only 1 node in the first level
  profile.resize(n + 1, 0);
  profile[0] = 1;

  //Initialize "linked list"
  for (int i = 0; i < n; i++) {
    p[i] = i + 1;
  }

  int l = 1, k = 0, j = 0;
  int tree_level = 1; //Variable used to keep track of the level of the tree
  bool new_level = true;
  bool try_value = false;
  bool retry = false;
  bool backtrack = false;
  while (l > 0) {
    //Some good prints for debugging
    //print_solution(x);
    //print_unused(p, j);
    if (new_level) {
      new_level = false;
      k = p[0];
      if (k == 0) { //List of unused numbers ended, finished building solution
        //print_solution(x);
        //solution_count++;
        backtrack = true; //Backtrack to find more solutions
      } else {
        j = 0; //Start from the beginning of unused list
        try_value = true;
        while (l <= 2*n &&
               x[l] < 0) { //Skip every negative number already put in solution
          if ((l == n / 2 && a[np] == false) ||
              (l >= n - 1 && a[2*n - l - 1] == false)) {
            //Don't need to try anything in this case, it won't work!
            //We would skip some negatives and end up too late to put 2*n - l - 1

            //Symmetry-breaking: if we got to n / 2 and a[np] is 0
            try_value = false;
            backtrack = true;
            break;
          } else {
            l++;
          }
        }
      }
    }
    if (try_value) {
      try_value = false;
      if (l + k + 1 > 2*n) {
        //Can't set x[l] = k, its pair would go out of bounds. Need to backtrack.
        backtrack = true;
      } else {
        //Optimization: break symmetries
        if (l == n / 2 && a[np] == false) {
          while (k != np) {
            j = k;
            k = p[k];
          }
        }
        //Optimization: 2n - l - 1 is not in yet, but this is its last chance
        if (l >= n - 1 && a[2*n - l - 1] == false) {
          while (l + k + 1 != 2*n) {
            j = k;
            k = p[k];
          } //Now k == 2n - l - 1, so we will try to put k in the solution
        }
        //Try to set x[l] = k. No immediate reason for this to be a problem.
        if (x[l + k + 1] == 0) { //The position of the pair of x[l] = k is unset
          x[l] = k;
          x[l + k + 1] = -k; //Set its pair in the solution
          a[k] = true; //Mark as used
          y[l] = j; //Record that we removed p[j] = k from the list at level l
          p[j] = p[k]; //Remove p[j] = k from the unused list
          profile[tree_level]++;
          l++;
          tree_level++;
          new_level = true;
        } else {
          //Cannot set x[l] = k, its pair is unavailable
          retry = true;
        }
      }
    }
    if (retry) {
      retry = false;
      j = k;
      k = p[j];
      if (k == 0) { //List ended, backtrack
        backtrack = true;
      } else { //try_value with the new j, k (next value in unused list)
        try_value = true;
      }
    }
    if (backtrack) {
      backtrack = false;
      l--;
      tree_level--;
      if (l > 0) {
        while (l > 0 && x[l] < 0) {
          l--; //Skip/Ignore all negative values in solution
        }
        //Remove x[l] from solution, put it back into the unused list
        k = x[l];
        x[l] = 0;
        a[k] = false;
        x[l + k + 1] = 0;
        j = y[l];
        p[j] = k;
        if (n % 2 == 0 && l == n / 2 && k == np) {
          backtrack = true;
        } else {
          retry = true;
        }
      }
    }
  }
}

//Currently doesn't work with estimate_cost.
size_t node_degree(std::vector<int>& X, std::vector<int>& y, int level) {
  int n  = y.size() / 2;
  size_t d = 0;
  for (int val = 1; val <= n; val++) {
    if (level + val + 1 < int(X.size()) && X[level + val + 1] == 0) { //The pair position is unused
      y[d] = val;
      d++;
    }
  }
  return d;
}

//3 Backtracking algorithms for Langford pairs
//1. simple
//2. with infeasibility prediction
//3. with infeasibility prediction and with dual-symmetry breaking
int main (int argc, char* argv[]) {
  if (argc < 3) {
    std::cout << "Expected algorithm and parameter n." << std::endl;
  } else {
    if (strcmp(argv[1], "langford1") == 0) {
      run_algorithm(langford1, std::stoi(argv[2]));
    } else if (strcmp(argv[1], "langford2") == 0) {
      run_algorithm(langford2, std::stoi(argv[2]));
    } else if (strcmp(argv[1], "langford3") == 0) {
      run_algorithm(langford3, std::stoi(argv[2]));
    } else if (strcmp(argv[1], "fournum") == 0) {
      int reps = 1000;
      if (argc >= 4) {
        reps = std::stoi(argv[3]);
      }
      fournum(2 * std::stoi(argv[2]), constant_cost, node_degree, reps);
    } else {
      std::cout << "Unknown algorithm." << std::endl;
    }
  }
  return 0;
}

#include <chrono>
#include <iostream>
#include <vector>

#include "tools.hpp"

typedef std::vector<size_t> Profile;
typedef std::vector<int> Solution;

bool bruteforce_test(Solution& s) {
  int n = s.size();
  for (int k = 0; k < n; k++) {
    for (int j = 0; j < k; j++) {
      if (s[j] == s[k]) { //Same column
        return false;
      }
      //Same diagonal1
      if (s[k] - k == s[j] - j) {
        return false;
      }
      //Same diagonal2
      if (s[k] + k == s[j] + j) {
        return false;
      }
    }
  }
  return true;
}

void bruteforce(int n, Profile& profile) {
  Solution sol(n);
  int l = 0;
  int t = 0;
  bool new_row = true;
  bool try_queen = false;
  bool retry = false;
  bool backtrack = false;
  profile[0] = 1;
  profile.push_back(0); //Add an extra int in profile to count solutions
  while (l >= 0) {
    if (new_row) { //B2 in Knuth's description
      new_row = false;
      if (l >= n) {
        //Solution is complete, check if it is valid
        if (bruteforce_test(sol)) {
          profile[n+1]++;
        }
        backtrack = true;
      } else {
        //Start trying to put a queen in the first column
        t = 0;
        try_queen = true;
      }
    }
    if (try_queen) { //B3 in Knuth's description
      try_queen = false;
      sol[l] = t;
      l++;
      profile[l]++;
      new_row = true;
    }
    if (retry) { //B4 in Knuth's description
      retry = false;
      if (t < n - 1) {
        t++;
        try_queen = true;
      } else {
        backtrack = true;
      }
    }
    if (backtrack) { //B5 in Knuth's description
      backtrack = false;
      l--;
      if (l >= 0) {
        t = sol[l];
        retry = true;
      }
    }
  }
}

//O(l) test to try to put queen in column t of l-th row
bool backtrack_test(Solution& s, int l, int t) {
  for (int i = 0; i < l; i++) {
    if (int(s[i]) == t) { //Same column, can't accept
      return false;
    }
    if (int(s[i]) - i == t - l) { //Same diagonal, can't accept
      return false;
    }
    if (int(s[i]) + i == t + l) { //Same diagonal, can't accept
      return false;
    }
  }
  return true;
}

//Backtrack without data structures to query conflicts.
void backtrack1(int n, Profile& profile) {
  Solution sol(n);
  int l = 0;
  int t = 0;
  bool new_row = true;
  bool try_queen = false;
  bool retry = false;
  bool backtrack = false;
  profile[0] = 1;
  while (l >= 0) {
    if (new_row) { //B2 in Knuth's description
      new_row = false;
      if (l >= n) {
        //Solution is complete, backtrack to get more
        backtrack = true;
      } else {
        //Start trying to put a queen in the first column
        t = 0;
        try_queen = true;
      }
    }
    if (try_queen) { //B3 in Knuth's description
      try_queen = false;
      if ( ! backtrack_test(sol, l, t) ) {
        //There is already a conflicting queen
        retry = true;
      }
      else {
        sol[l] = t;
        l++;
        profile[l]++;
        new_row = true;
      }
    }
    if (retry) { //B4 in Knuth's description
      retry = false;
      if (t < n - 1) {
        t++;
        try_queen = true;
      } else {
        backtrack = true;
      }
    }
    if (backtrack) { //B5 in Knuth's description
      backtrack = false;
      l--;
      if (l >= 0) {
        t = sol[l];
        retry = true;
      }
    }
  }
}

//Backtrack with data structures to query conflicts in O(1)
void backtrack2(int n, Profile& profile) {
  std::vector<bool> a(n, false);
  std::vector<bool> b(2*n - 1, false);
  std::vector<bool> c(2*n - 1, false);
  Solution sol(n);
  int l = 0;
  int t = 0;
  bool new_row = true;
  bool try_queen = false;
  bool retry = false;
  bool backtrack = false;
  profile[0] = 1;
  while (l >= 0) {
    if (new_row) { //B2 in Knuth's description
      new_row = false;
      if (l >= n) {
        //Solution is complete, backtrack to get more
        backtrack = true;
      } else {
        //Start trying to put a queen in the first column
        t = 0;
        try_queen = true;
      }
    }
    if (try_queen) { //B3 in Knuth's description
      try_queen = false;
      if (a[t] || b[t + l] || c[t - l + n + 1]) {
        //There is already a conflicting queen
        retry = true;
      }
      else {
        a[t] = true;
        b[t + l] = true;
        c[t - l + n + 1] = true;
        sol[l] = t;
        l++;
        profile[l]++;
        new_row = true;
      }
    }
    if (retry) { //B4 in Knuth's description
      retry = false;
      if (t < n - 1) {
        t++;
        try_queen = true;
      } else {
        backtrack = true;
      }
    }
    if (backtrack) { //B5 in Knuth's description
      backtrack = false;
      l--;
      if (l >= 0) {
        t = sol[l];
        c[t - l + n + 1] = false;
        b[t + l] = false;
        a[t] = false;
        retry = true;
      }
    }
  }
}

//Backtrack using gotos and data structures just like in Knuth.
void backtrack3(int n, Profile& profile) {
  std::vector<bool> a(n, false);
  std::vector<bool> b(2*n - 1, false);
  std::vector<bool> c(2*n - 1, false);
  Solution sol(n);
  int l = 0;
  int t = 0;
  profile[0] = 1;
 B2: //Enter row l
  if (l >= n) {
    //sol is complete
    goto B5;
  } else {
    t = 0;
  }
 B3: //Try queen at position t in row l
  if (a[t] || b[t + l] || c[t - l + n + 1])
    //There is already a conflicting queen
    goto B4;
  else {
    a[t] = true;
    b[t + l] = true;
    c[t - l + n + 1] = true;
    sol[l] = t;
    l++;
    profile[l]++;
    goto B2;
  }
 B4: //Try again for other positions t
  if (t < n - 1) {
    t++;
    goto B3;
  }
 B5: //Backtrack
  l--;
  if (l >= 0) {
    t = sol[l];
    c[t - l + n + 1] = false;
    b[t + l] = false;
    a[t] = false;
    goto B4;
  }
}

void walker(int n, Profile& profile) {
  int l = 1;
  int t = 0;
  bool new_row = true;
  bool try_queen = false;
  bool backtrack = false;

  //Bit vectors for Walker's algorithm
  //1-indexed to have easier formulas
  std::vector<unsigned> a(n + 1, 0), b(n + 1, 0), c(n + 1, 0), s(n + 1, 0);
  const unsigned mask = (1 << n) - 1; //n bits 1

  profile[0] = 1;
  while (l > 0) {
    if (new_row) { //W2 in Knuth's description
      new_row = false;
      if (l > n) {
        //Solution is complete, backtrack to get more
        backtrack = true;
      } else {
        //Start trying to put a queen in the first column
        try_queen = true;
        s[l] = mask & (~a[l - 1]) & (~b[l - 1]) & (~c[l - 1]);
      }
    }
    if (try_queen) { //W3 in Knuth's description
      try_queen = false;
      if ( s[l] != 0 ) { //There's something in the set s[l]
        //Updating Walker's algorithm structures
        t = s[l] & (-s[l]); //Get minimum element of s
        a[l] = a[l - 1] + t; //Now this row is occupied
        b[l] = (b[l - 1] + t) >> 1; //Move the diagonal 1 to the left
        c[l] = ((c[l - 1] + t) << 1) & mask; //Move diagonal 1 to the right
        s[l] = s[l] - t; //Remove t from set

        profile[l]++;
        l++;
        new_row = true;
      } else {
        backtrack = true;
      }
    }
    if (backtrack) { //W4 in Knuth's description
      backtrack = false;
      l--;
      if (l > 0) {
        try_queen = true;
      }
    }
  }
}

size_t node_degree(std::vector<int>& X, std::vector<int>& y, int level) {
  size_t d = 0;
  int n = y.size();
  //Get current row
  int row = level;
  for (int col = 0; col < n; col++) {
    if ( backtrack_test(X, row, col) ) { //Passes the test
      y[d] = col;
      d++;
    }
  }
  return d;
}

int main (int argc, char* argv[]) {
  if (argc <= 2) {
    std::cout << "Not enough parameters. Expected algorithm and board size."
              << std::endl;
  } else {
    std::string algorithm(argv[1]);
    int n = std::stoi(argv[2]);
    if (algorithm == "bruteforce") {
      run_algorithm(bruteforce, n);
    } else if (algorithm == "backtrack1") {
      run_algorithm(backtrack1, n);
    } else if (algorithm == "backtrack2") {
      run_algorithm(backtrack2, n);
    } else if (algorithm == "walker") {
      run_algorithm(walker, n);
    } else if (algorithm == "fournum") {
      int reps = 1000;
      if (argc >= 4) {
        reps = std::stoi(argv[3]);
      }
      fournum(n, constant_cost, node_degree, reps);
    } else if (algorithm == "solutions") {
      //The method implemented here doesn't work.
      //This happens because the tree leads to dead ends, and rejection sampling
      //is not enough to deal with this problem.
      int reps = 1000;
      if (argc >= 4) {
        reps = std::stoi(argv[3]);
      }
      estimate_solutions(n, reps, constant_cost, node_degree);
    } else {
      std::cout << "Unknown algorithm, exiting." << std::endl;
    }
  }
  return 0;
}
